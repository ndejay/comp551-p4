import csv
import os
import pandas as pd
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score, log_loss

## I can't test it so I'm not 100% sure it'll work

train = pd.read_csv("data/dbpedia_train_tf-idf.csv", header=None)
cat = train.columns[len(train.columns)-1]
id = train.columns[0]
daty = train[cat]
train.drop(cat, axis=1, inplace=True)
train.drop(id, axis=1, inplace=True)
datx = train

test = pd.read_csv("data/dbpedia_test_tf-idf.csv", header=None)
cat = test.columns[len(test.columns)-1]
id = test.columns[0]
lab = test[cat]
test.drop(cat, axis=1, inplace=True)
test.drop(id, axis=1, inplace=True)

nb_classifier = GaussianNB()
nb_classifier.fit(datx, daty)
preds = nb_classifier.predict(test)
accuracy_score(preds,lab)
log_loss(preds,lab)
