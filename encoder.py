import pandas as pd
import numpy as np

from collections import Counter, OrderedDict

import sys
import csv
maxInt = sys.maxsize
decrement = True

while decrement:
    # decrease the maxInt value by factor 10 
    # as long as the OverflowError occurs.

    decrement = False
    try:
        csv.field_size_limit(maxInt)
    except OverflowError:
        maxInt = int(maxInt/10)
        decrement = True

def form_ranks(text):        
    text = text.lower()
    c = Counter()
    c += Counter(text.strip())
    return c

def compress_code(d):
    tup = sorted(dict(d).items(), key=lambda x:x[1], reverse = True)
    freq_d = dict((x, y) for x, y in tup)
    x = OrderedDict(freq_d)
    rank_d = {}
    for key,value in freq_d.items():
        rank_d[key] = list(x.keys()).index(key)
    for key, value in rank_d.items():
        rank_d[key] = int('1'+value*'0'+'1')
    return rank_d

def form_matrix(words, compressed):
    matrix = np.zeros((128,256), dtype=np.int)
    for word in range(min(len(words), 128)):
        letters = list(words[word])
        codes = [str(compressed[letters[letter]]) for letter in range(len(letters))]
        codes = ''.join(codes)
        length = min(len(codes), 256)
        matrix[word][:length] = list(codes[:length])
    return matrix

def convert(in_file, out_file, encoding):
    print("FIRST PASS")
    # First pass: calculate global ranks
    global_rank = Counter()
    with open(in_file, encoding=encoding, errors='ignore') as csvfile:
        handle = csv.reader(csvfile, delimiter=',')
        for row in handle:
            global_rank += form_ranks(row[2])
    global_rank = compress_code(global_rank)
    print(global_rank)

    print("SECOND PASS")
    # Second pass: encode observation at a time
    encoded_matrix = []
    counter = 0
    with open(in_file, encoding=encoding, errors='ignore') as csvfile:
        handle = csv.reader(csvfile, delimiter=',')
        with open(out_file, 'w') as o:
           for row in handle:
               if counter % 100 == 0:
                   print(counter)
               label = row[0]
               text  = form_matrix(row[2].lower().split(), global_rank)
               text  = [str(z) for z in text.reshape((128*256)).tolist()]
               text  = ''.join(text)
               o.write("{},{}\n".format(label, text)) 
               counter += 1

convert("ag_news_test.csv", "ag_news_test.encoded.csv", encoding ="ascii")
