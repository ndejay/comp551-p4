from __future__ import print_function
import keras
from keras.layers import Input, Conv1D, MaxPooling1D, Dense, Dropout, Activation, Flatten, Merge, Concatenate, Add
from keras.models import Model
from keras.optimizers import Adam
from keras.utils import np_utils
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.pylab as plt
import numpy as np
import pandas as pd

###############################################################################
# Define helper functions
###############################################################################

def load_encoding(filename, num_classes, start = 0, end = 999999999):
    label = []
    data = []
    counter = 0
    with open(filename) as file:
      for l in file:
        #if counter % 1000 == 0:
        #    print("load_encoding({}, {}, {}, {}) @ {}".format(filename, num_classes, start, end, counter))
        if counter < start:
            counter += 1
            continue
        elif counter >= end:
            break
        else:
            (l, d) = l.strip().split(',')
            label.append(l)
            data.append(list(d))
            counter += 1
    label = np.array(label).astype('int')
    data  = np.array(data).astype('int')
    num_obs= len(label)
    if num_obs == 0:
      return (label, data)
    label  = label - 1
    label2 = keras.utils.to_categorical(label, num_classes)
    data   = data.reshape((num_obs, 128, 256))
    return (label2, data)

class AccuracyHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.acc = []
    def on_epoch_end(self, batch, logs={}):
        self.acc.append(logs.get('acc'))

###############################################################################
# Model construction
###############################################################################

#
# agnews
#

# train_fn    = 'data/train_ag_news.encoded.csv'
# test_fn     = 'data/test_ag_news.encoded.csv'
# train_fn = 'data/ag_news_train.tf-idf.csv'
# test_fn  = 'data/ag_news_test.tf-idf.csv'
# num_classes = 4

#
# dbpedia 
#

train_fn    = 'data/train_yelp_polarity.encoded.csv'
test_fn     = 'data/test_yelp_polarity.encoded.csv'
num_classes = 2

#
# model
#

input_shape = (128, 256)
superbatch_size = 100000
batch_size = 32
epochs = 5

input_1         = Input(shape=input_shape)
conv1d_1        = Conv1D(256, kernel_size=3)(input_1)
conv1d_2        = Conv1D(256, kernel_size=3)(input_1)
conv1d_3        = Conv1D(256, kernel_size=3)(input_1)
conv1d_4        = Conv1D(256, kernel_size=3)(input_1)
max_pooling1d_1 = MaxPooling1D(pool_size=3)(conv1d_1)
max_pooling1d_2 = MaxPooling1D(pool_size=3)(conv1d_2)
max_pooling1d_3 = MaxPooling1D(pool_size=3)(conv1d_3)
max_pooling1d_4 = MaxPooling1D(pool_size=3)(conv1d_4)
# This one was a bit tricky to figure out, since merge has been made
# obsolete in recent version of keras.  The axis is not the same as
# the one specified in the documentation.
merge_1         = Concatenate(axis=1)([max_pooling1d_1, max_pooling1d_2, max_pooling1d_3, max_pooling1d_4])
conv1d_5        = Conv1D(256, kernel_size=5)(merge_1)
max_pooling1d_5 = MaxPooling1D(pool_size=3)(conv1d_5)
conv1d_6        = Conv1D(256, kernel_size=5)(max_pooling1d_5)
max_pooling1d_6 = MaxPooling1D(pool_size=4)(conv1d_6)
flatten_1       = Flatten()(max_pooling1d_6)
dense_1         = Dense(128)(flatten_1)
dense_2         = Dense(num_classes)(dense_1)

adam            = Adam(lr=10e-3, beta_1=0.9, beta_2=0.999, epsilon=10e-8) # default decay?

model = Model(inputs=input_1, outputs=dense_2)

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=adam,
              metrics=['accuracy'])

model.summary()

history = AccuracyHistory()

###############################################################################
# Model training
###############################################################################

for epoch in range(1, epochs + 1):
  print('epoch={}'.format(epoch))
  (start, end) = (0, superbatch_size)
  while True:
    (my_y_train, my_x_train) = load_encoding(train_fn, num_classes, start, end)
    if len(my_x_train) != 0:
      print('epoch={};obs={}-{}'.format(epoch, start, end))
      model.fit(my_x_train, my_y_train,
          batch_size=batch_size,
          epochs=1,
          verbose=1,
          #validation_data=(my_x_train, my_y_train),
          callbacks=[history])
      (start, end) = (end, end + superbatch_size)
    else:
      break

(y_test, x_test) = load_encoding(test_fn, num_classes)
model.evaluate(x_test, y_test, batch_size=batch_size)
